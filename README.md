Console Utilities
=================

Utilities for facilitating interactive command line interfaced (cli) targeted
mainly at utilities used in the linux terminals.

See [docs/index.md](docs/index.md) for details.